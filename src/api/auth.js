import Api from './config';

export const signIn = ({ email, password }) =>
  Api().post('/users/auth/sign_in', { email, password });
