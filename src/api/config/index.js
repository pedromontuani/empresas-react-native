import axios from 'axios';
import { API_URL } from '~/config';
import { setLoading } from '~/store/modules/loader/actions';
import store from '~/store';

export default () => {
  const state = store.getState();
  const { accessToken, uid, client } = state.auth;
  const instance = axios.create({
    baseURL: API_URL,
    headers: {
      'access-token': accessToken,
      uid,
      client,
    },
  });

  store.dispatch(setLoading(true));
  instance.interceptors.request.use(
    (config) => config,
    (error) => {
      store.dispatch(setLoading(false));
      return Promise.reject(error);
    },
  );

  instance.interceptors.response.use(
    (response) => {
      store.dispatch(setLoading(false));
      return response;
    },
    (error) => {
      store.dispatch(setLoading(false));
      return Promise.reject(error);
    },
  );

  return instance;
};
