import Api from './config';

export const getAllEnterprises = () => Api().get('/enterprises');

export const getEnterprisesByFilter = ({ type, name }) =>
  Api().get('/enterprises', {
    params: { enterprise_types: type, name },
  });

export const getEnterpriseById = (id) => Api().get(`/enterprises/${id}`);
