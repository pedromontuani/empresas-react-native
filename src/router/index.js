/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector, useDispatch } from 'react-redux';

import authSelector from '~/store/modules/auth/selectors';
import { offlineLogin } from '~/store/modules/auth/actions';

import Loading from '~/components/Loading/';

import SignInScreen from '~/screens/SignInScreen';
import EnterprisesScreen from '~/screens/EnterprisesScreen';
import EnterpriseInfoScreen from '~/screens/EnterpriseInfoScreen';

const Router = () => {
  const Stack = createStackNavigator();
  const dispach = useDispatch();
  const isAuthenticated = useSelector(authSelector.isAuthenticated);
  const authLoading = useSelector(authSelector.isLoadingOffline);

  useEffect(() => {
    dispach(offlineLogin());
  }, []);

  return authLoading ? (
    <Loading />
  ) : (
    <NavigationContainer>
      <Stack.Navigator>
        {!isAuthenticated ?
          <Stack.Screen name="SignIn" component={SignInScreen} options={{ headerShown: false }} />
          :
          <>
            <Stack.Screen
              name="Enterprises"
              component={EnterprisesScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="EnterpriseInfo"
              component={EnterpriseInfoScreen}
              options={{ headerShown: false }}
            />
          </>}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Router;
