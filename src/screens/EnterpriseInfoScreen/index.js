import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import SocialIcon from 'react-native-vector-icons/FontAwesome';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useSelector } from 'react-redux';
import loadingSelector from '~/store/modules/loader/selectors';
import Loading from '~/components/Loading';
import { BASE_URL } from '~/config';

import styles, { gradient } from './styles';

import { getEnterpriseById } from '~/api/enterprises';

const EnterpriseInfoScreen = ({ navigation, route }) => {
  const [enterpriseData, setEnterpriseData] = useState({});
  const [socialMedias, setSocialMedias] = useState([]);
  const [isReady, setIsReady] = useState(false);
  const isLoading = useSelector(loadingSelector.isVisible);
  const socialMediaTypes = ['facebook', 'linkedin', 'twitter', 'phone'];
  const { enterpriseId } = route.params;

  const getEnterprisePhoto = () => `${BASE_URL}${enterpriseData.photo}`;

  const formatLocation = () =>
    `${enterpriseData.city}, ${enterpriseData.country}`;

  const formatMoney = (money) => Number.parseFloat(money).toFixed(2);

  const iconDisabled = ({ uri }) => !uri;

  const openUrl = ({ uri }) => Linking.openURL(uri);

  const Detail = ({ text, icon }) => (
    <View style={styles.detail}>
      <Icon style={styles.detailIcon} name={icon} />
      <Text style={styles.detailText}>{text}</Text>
    </View>
  );

  useEffect(() => {
    getEnterpriseById(enterpriseId)
      .then(({ data }) => {
        setEnterpriseData(data.enterprise);
        const social = [];
        socialMediaTypes.forEach((key) => {
          social.push({
            type: key,
            uri: data.enterprise[key],
          });
        });
        setSocialMedias(social);
      })
      .finally(() => {
        setIsReady(true);
      });
  }, []);

  return isLoading || !isReady ? (
    <Loading />
  ) : (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <LinearGradient colors={gradient} style={styles.customNavBar}>
          <TouchableOpacity
            style={styles.backButtonContainer}
            onPress={() => navigation.goBack()}
          >
            <Icon style={styles.backButtonIcon} name="arrow-back" />
          </TouchableOpacity>
        </LinearGradient>
        <ScrollView style={styles.contentContainer}>
          <Image
            style={styles.image}
            source={{ uri: getEnterprisePhoto() }}
            resizeMode="cover"
          />
          <View style={styles.content}>
            <Text style={styles.title}>{enterpriseData.enterprise_name}</Text>
            <View style={styles.detailsContainer}>
              <Detail icon="location-pin" text={formatLocation()} />
              <Detail
                icon="business"
                text={enterpriseData.enterprise_type?.enterprise_type_name}
              />
              <Detail
                icon="attach-money"
                text={formatMoney(enterpriseData.share_price)}
              />
            </View>
            <Text style={styles.description}>{enterpriseData.description}</Text>
          </View>
        </ScrollView>
        <View style={styles.footer}>
          {socialMedias.map((social) => (
            <TouchableOpacity
              key={social.type}
              onPress={() => openUrl(social)}
              disabled={() => iconDisabled(social)}
              style={styles.socialIconContainer}
            >
              <SocialIcon
                style={[
                  styles.socialIcon,
                  iconDisabled(social) && styles.socialIconDisabled,
                ]}
                name={social.type}
              />
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default EnterpriseInfoScreen;
