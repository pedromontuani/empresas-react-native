/* eslint-disable camelcase */
import React, { useState, useEffect } from 'react';
import { Animated, Easing, RefreshControl } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import authSelector from '~/store/modules/auth/selectors';
import loadingSelector from '~/store/modules/loader/selectors';

import MainHeader from '~/components/MainHeader';
import Card from '~/components/Card';
import FAB from '~/components/FloatingActionButton';
import SelectTypeModal from '~/components/SelectTypeModal';
import NoData from '~/components/NoData';

import { logout } from '~/store/modules/auth/actions';
import { getAllEnterprises, getEnterprisesByFilter } from '~/api/enterprises';
import colors from '~/theme/colors';
import styles from './styles';
import variables from '~/theme/variables';

const EnterprisesScreen = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [enterpriseTypes, setEnterpriseTypes] = useState([]);
  const [selectedType, setSelectedType] = useState(undefined);
  const [enterprises, setEnterprises] = useState(undefined);
  const [searchValue, setSearchValue] = useState('');
  const loading = useSelector(loadingSelector.isVisible);
  const user = useSelector(authSelector.getUser);
  const scrollY = new Animated.Value(0);

  const dispatch = useDispatch();

  const headerHeight = scrollY.interpolate({
    inputRange: [0, 300],
    outputRange: [variables.headerMaxHeight, variables.headerMinHeight],
    extrapolate: 'clamp',
    easing: Easing.quad,
  });

  const getEnterprises = () => {
    getAllEnterprises().then(({ data }) => {
      setEnterprises(data.enterprises);
    });
  };

  const getByFilter = () => {
    getEnterprisesByFilter({
      name: searchValue || undefined,
      type: selectedType?.id,
    }).then(({ data }) => {
      setEnterprises(data.enterprises);
    });
  };

  const onPressFAB = () => {
    setModalVisible(true);
  };

  const onPressCard = (enterprise) => {
    navigation.navigate('EnterpriseInfo', { enterpriseId: enterprise.id });
  };

  const refreshData = () => {
    if (!!searchValue.trim() || !!selectedType) {
      getByFilter();
    } else {
      if (!searchValue) {
        getEnterprises();
      }
      setSearchValue('');
    }
  };

  const signOut = () => {
    dispatch(logout());
  };

  const listIsEmpty = () => enterprises?.length === 0;

  const listIsNotEmpty = () => enterprises?.length > 0;

  const showNoData = () => !loading && listIsEmpty();

  const renderItem = ({ item }) => (
    <Card
      imageUrl={item.photo}
      title={item.enterprise_name}
      description={item.description}
      onPress={() => onPressCard(item)}
    />
  );

  useEffect(() => {
    getEnterprises();
  }, []);

  useEffect(() => {
    if (enterpriseTypes.length === 0 && listIsNotEmpty()) {
      const types = {};
      enterprises.forEach(({ enterprise_type }) => {
        const { id, enterprise_type_name } = enterprise_type;
        types[enterprise_type_name] = { id };
      });
      const formattedTypes = Object.keys(types).map((type) => ({
        name: type,
        id: types[type].id,
      }));
      const sortedTypes = formattedTypes.sort((a, b) => {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
      const defaultType = { name: 'All types', id: undefined };
      setEnterpriseTypes([defaultType, ...sortedTypes]);
    }
  }, [enterprises, enterpriseTypes]);

  return (
    <SafeAreaView style={styles.container}>
      <Animated.View style={styles.container}>
        <MainHeader
          user={user}
          filterLabel={selectedType?.id && selectedType?.name}
          headerHeight={headerHeight}
          searchValue={searchValue}
          setSearchValue={setSearchValue}
          onSubmitSearch={refreshData}
          onSignOut={signOut}
        />
        {showNoData() ? (
          <NoData />
        ) : (
          <Animated.FlatList
            refreshControl={
              <RefreshControl
                colors={[colors.primary]}
                refreshing={loading}
                onRefresh={refreshData}
              />
            }
            style={styles.container}
            contentContainerStyle={styles.flatListContent}
            data={enterprises}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
            scrollEventThrottle={1}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: scrollY } } }],
              { useNativeDriver: false },
            )}
          />
        )}

        <FAB onPress={onPressFAB} />
        <SelectTypeModal
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          onConfirm={refreshData}
          enterpriseTypes={enterpriseTypes}
          selectedType={selectedType}
          setSelectedType={setSelectedType}
        />
      </Animated.View>
    </SafeAreaView>
  );
};

export default EnterprisesScreen;
