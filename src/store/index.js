import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';
import rootSaga from './sagas';

const middlewares = [];

const sagaMiddleware = createSagaMiddleware();

middlewares.push(sagaMiddleware);

const enhancer = applyMiddleware(...middlewares);

const store = createStore(reducers, enhancer);

sagaMiddleware.run(rootSaga);

export default store;
