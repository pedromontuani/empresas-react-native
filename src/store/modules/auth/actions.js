import ActionTypes from '~/store/actionTypes';

export const offlineLogin = () => ({
  type: ActionTypes.AUTH_OFFLINE_LOGIN,
});

export const login = (email, password) => ({
  type: ActionTypes.AUTH_LOGIN,
  email,
  password,
});

export const authLoading = (isLoading) => ({
  type: ActionTypes.AUTH_LOADING,
  isLoading,
});

export const authLoadingOffline = (isLoading) => ({
  type: ActionTypes.AUTH_OFFLINE_LOADING,
  isLoading,
});

export const loginSuccess = ({ accessToken, uid, client }) => ({
  type: ActionTypes.AUTH_LOGIN_SUCCESS,
  accessToken,
  uid,
  client,
});

export const logout = () => ({
  type: ActionTypes.AUTH_LOGOUT,
});

export const logoutSuccess = () => ({
  type: ActionTypes.AUTH_LOGOUT_SUCCESS,
});

export const setUser = (user) => ({
  type: ActionTypes.AUTH_SET_USER,
  user,
});
