import produce from 'immer';

import ActionTypes from '~/store/actionTypes';

const initialState = {
  accessToken: null,
  uid: null,
  client: null,
  user: null,
  isAuthenticated: null,
  isLoading: null,
  isLoadingOffline: true,
};

const reducers = (state, action) => {
  state = state || initialState;
  switch (action.type) {
    case ActionTypes.AUTH_LOADING:
      return produce(state, (draft) => {
        draft.isLoading = action.isLoading;
      });
    case ActionTypes.AUTH_OFFLINE_LOADING:
      return produce(state, (draft) => {
        draft.isLoadingOffline = action.isLoadingOffline;
      });
    case ActionTypes.AUTH_LOGIN_SUCCESS:
      return produce(state, (draft) => {
        draft.accessToken = action.accessToken;
        draft.uid = action.uid;
        draft.client = action.client;
        draft.isAuthenticated = true;
      });
    case ActionTypes.AUTH_SET_USER:
      return produce(state, (draft) => {
        draft.user = action.user;
      });
    case ActionTypes.AUTH_LOGOUT_SUCCESS:
      return produce(state, (draft) => {
        draft.user = null;
        draft.accessToken = null;
        draft.uid = null;
        draft.client = null;
        draft.isAuthenticated = false;
      });
    default:
      return state;
  }
};

export { reducers };
