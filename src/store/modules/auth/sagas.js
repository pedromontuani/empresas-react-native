import { call, put, all, takeLatest } from 'redux-saga/effects';
import EncryptedStorage from 'react-native-encrypted-storage';
import Toast from 'react-native-simple-toast';
import {
  loginSuccess,
  setUser,
  logoutSuccess,
  authLoading,
  authLoadingOffline,
} from './actions';
import { signIn } from '~/api/auth';
import ActionTypes from '~/store/actionTypes';

function* offlineLogin() {
  yield put(authLoadingOffline(true));
  try {
    const oauthData = yield call(EncryptedStorage.getItem, 'oauth_data');
    const userData = yield call(EncryptedStorage.getItem, 'user_data');
    if (!!oauthData && !!userData) {
      const { uid, accessToken, client } = JSON.parse(oauthData);
      const user = JSON.parse(userData);
      yield put(loginSuccess({ uid, accessToken, client }));
      yield put(setUser(user));
    }
  } catch (e) {
    console.log(e);
  }
  yield put(authLoadingOffline(false));
}

function* login({ email, password }) {
  yield put(authLoading(true));
  try {
    const { headers, data } = yield call(signIn, {
      email,
      password,
    });

    const { uid, 'access-token': accessToken, client } = headers;

    yield call(
      EncryptedStorage.setItem,
      'oauth_data',
      JSON.stringify({ uid, accessToken, client }),
    );
    yield call(EncryptedStorage.setItem, 'user_data', JSON.stringify(data));
    yield put(loginSuccess({ uid, accessToken, client }));
    yield put(setUser(data));
  } catch (e) {
    console.log(e);
    const { response } = e;
    if (response?.data?.errors?.length > 0) {
      yield call(Toast.show, response.data.errors[0]);
    } else {
      yield call(Toast.show, 'An error ocurred. Please try again.');
    }
  }
  yield put(authLoading(false));
}
function* logout() {
  yield put(authLoading(true));
  try {
    yield call(EncryptedStorage.clear);
    yield put(logoutSuccess());
  } catch (e) {
    console.log(e);
  }
  yield put(authLoading(false));
}

export default all([
  takeLatest(ActionTypes.AUTH_OFFLINE_LOGIN, offlineLogin),
  takeLatest(ActionTypes.AUTH_LOGIN, login),
  takeLatest(ActionTypes.AUTH_LOGOUT, logout),
]);
