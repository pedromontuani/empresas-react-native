const selectors = {
  getToken: (state) => state.auth.accessToken,
  getUid: (state) => state.auth.uid,
  getClient: (state) => state.auth.client,
  getUser: (state) => state.auth.user,
  isAuthenticated: (state) => state.auth.isAuthenticated,
  isLoading: (state) => state.auth.isLoading,
  isLoadingOffline: (state) => state.auth.isLoadingOffline,
};

export default selectors;
