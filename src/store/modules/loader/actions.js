import ActionTypes from '~/store/actionTypes';

export const setLoading = (isVisible) => ({
  type: ActionTypes.SET_LOADING,
  isVisible,
});
