import produce from 'immer';

import ActionTypes from '~/store/actionTypes';

export const initialState = {
  visible: false,
};

const reducers = (state, action) => {
  state = state || initialState;
  if (action.type === ActionTypes.SET_LOADING) {
    return produce(state, (draft) => {
      draft.visible = action.isVisible ? action.isVisible : !state.visible;
    });
  }
  return state;
};

export { reducers };
