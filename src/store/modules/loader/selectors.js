const selectors = {
  isVisible: (state) => state.loader.visible,
};

export default selectors;
