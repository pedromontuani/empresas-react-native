import { combineReducers } from 'redux';
import { reducers as auth } from './modules/auth/reducers';
import { reducers as loader } from './modules/loader/reducers';

const reducers = combineReducers({
  auth,
  loader,
});

export default reducers;
